#include "class.h"

int class_para  =  0;

typedef unsigned char  uint8_t;
typedef char           int8_t;
typedef unsigned short uint16_t;
typedef short          int16_t;
typedef unsigned int   uint32_t;
typedef int            int32_t;


uint8_t u8a;
uint8_t u8b;
uint8_t u8c;
int8_t  s8a;
int8_t  s8b;
uint16_t u16a;
int16_t  s16a;
uint16_t u16b;
int16_t  s16b;
uint32_t u32a;
int32_t  s32a;
int32_t  s32b;
char*   pca;
float   f32a;
float   f32b;
double  f64a;
double  f64b;

extern void foo1(uint8_t u8)
{
    return;
}
int16_t func(void)
{
    foo1(u8a);          /*compliant*/
    foo1(u8a + u8b);    /*compliant*/
    foo1(s8a);          /*not compliant*/
    foo1(u16a);         /*not compliant*/
    foo1(2);            /*not compliant有符号->无符号*/
    foo1(2U);           /*compliant*/
    foo1((uint8_t)2);   /*compliant有符号->无符号，强制类型转换*/
    s8a + u8a;          /*not compliant有符号->无符号*/
    s8a + (int8_t)u8a;  /*compliant强制类型转换*/
    s8b = u8a;          /*not compliant*/
    u8a + 5;            /*not compliant有符号->无符号*/
    u8a + 5U;           /*compliant*/
    u8a + (uint8_t)5;   /*compliant*/
    u8a = u16a;         /*not compliant数值截断*/
    u8a = (uint8_t)u16a;/*compliant*/
    u8a = 5UL;          /*not compliant数值截断*/
    u8a + 10UL;         /*compliant uchar->ulong*/
    u8a = 5U;           /*compliant*/
    u8a >> 3;           /*compliant*/
    u8a >> 3U;          /*compliant*/
    pca = "p";          /*compliant*/
    s32a + 80000;       /*compliant*/
    s32a + 80000L;      /*compliant*/
    f32a = f64a;        /*not compliant数值截断*/
    f32a = 2.5;         /*not compliant没有后缀默认是double*/
    f32a = 2.5f;        /*compliant*/
    u8a = u8b + u8c;    /*compliant*/
    s16a = u8b + u8b;   /*not compliant无符号->有符号*/
    s32a = u8b + u8c;   /*not compliant*/
    u8a = f32a;         /*not compliant*/
    s32a = 1.0;         /*not compliant*/
    f32a = 1;           /*not compliant整形->浮点*/
    f32a = s16a;        /*not compliant*/
    f32a + 1;           /*not compliant*/
    f64a * s32a;        /*not compliant*/
    return(s32a);       /*not compliant*/
    return(s16a);       /*compliant*/
    return(20000);      /*compliant*/
    return(20000L);     /*not compliant*/
    return(s8a);        /*not compliant返回的表达式*/
    return(u16a);       /*not compliant有符号->无符号*/
}

int16_t foo2(void)
{
    (u16a + u16b)+ u32a;   /*not compliant第1个加号是16位运算*/
    s32a + s8a + s8b;       /*compliant*/
    s8a + s8b + s32a;       /*not compliant第1个加号是8位运算*/
    f64a = f32a + f32b;     /*not compliant加法是32位运算可能溢出*/
    f64a = f64b + f32a;     /*compliant*/
    f64a = s32a / s32b;     /*not compliant除法是32位运算*/
    u32a = u16a + u16a;     /*not compliant加法是32位运算*/
    s16a = s8a;             /*compliant*/
    s16a = s16b + 20000;    /*compliant*/
    s32a = s16a + 20000;    /*not compliant加法是16位运算*/
    s32a = s16a + (int32_t)20000;/*compliant*/
    u16a = u16b + u8a;      /*compliant*/
    foo1(u16a);             /*not compliant数值截断*/
    foo1(u8a + u8b);        /*compliant*/
    return s16a;            /*compliant*/
    return s8a;             /*not compliant返回的表达式*/
}

int16_t foo3(void)
{
    (float)(f64a + f64b);       /*compliant*/
    (double)(f32a + f32b);      /*not compliant复杂表达式不允许往更宽的类型强制转换*/
    (double)f32a;               /*compliant但是基本可类型可以*/
    (double)(s32a / s32b);      /*not compliant*/
    (double)(s32a > s32b);      /*not compliant*/
    (double)s32a / (float)s32b; /*compliant*/
    (uint32_t)(u16a + u16b);    /*not compliant*/
    (uint32_t)u16a + u16b;      /*compliant*/
    (uint32_t)u16a + (uint32_t)u16b;/*compliant*/
    (int16_t)(s32a - 12345);    /*compliant*/
    (uint8_t)(u16a * u16b);     /*compliant复杂表达式可以往更窄的类型强制转换*/
    (uint16_t)(u8a * u8b);      /*not compliant但是不允许往更宽的类型强制转换*/
    (int16_t)(s32a * s32b);     /*compliant*/
    (int32_t)(s16a * s16b);     /*not compliant*/
    (uint16_t)(f64a + f64b);    /*not compliant浮点类型的复杂表达式只能强制转换到更窄的浮点型*/
    (float)(u16a + u16b);       /*not compliant*/
    return 0;
}




